from sqlalchemy import Table, Boolean, Column, ForeignKey, Integer, String, DateTime
from sqlalchemy.orm import relationship

from .database import Base


task_association_table = Table(
    "task_association",
    Base.metadata,
    Column("task_id", Integer, ForeignKey("tasks.id")),
    Column("user_id", Integer, ForeignKey("users.id")),
)


class User(Base):
    __tablename__ = "users"

    id = Column(Integer, primary_key=True, index=True)
    name = Column(String)
    surname = Column(String)
    email = Column(String, unique=True, index=True)
    password = Column(String)
    org_id = Column(Integer, ForeignKey("organizations.id"))
    is_admin = Column(Boolean, default=False)

    tasks = relationship(
        "Task", secondary=task_association_table, back_populates="assignees"
    )
    org = relationship("Organization", back_populates="users")


class Task(Base):
    __tablename__ = "tasks"

    id = Column(Integer, primary_key=True, index=True)
    title = Column(String, index=True)
    description = Column(String, index=True)
    deadline = Column(DateTime)
    status = Column(String)
    org_id = Column(Integer, ForeignKey("organizations.id"))

    org = relationship("Organization", back_populates="tasks")
    assignees = relationship(
        "User", secondary=task_association_table, back_populates="tasks"
    )


class Organization(Base):
    __tablename__ = "organizations"

    id = Column(Integer, primary_key=True, index=True)
    name = Column(String)
    phone = Column(String)
    address = Column(String)

    users = relationship("User", back_populates="org")
    tasks = relationship("Task", back_populates="org")