from datetime import datetime
from typing import List, Optional

from pydantic import BaseModel


# === user ===

class UserBase(BaseModel):
    name: str
    surname: str
    email: str
    is_admin: Optional[bool] = False


class UserCreate(UserBase):
    password: str


class User(UserBase):
    id: int
    org_id: Optional[int]
    # tasks: "List[Task]" = []
    org: Optional["Organization"]

    class Config:
        orm_mode = True


# === task ===

class TaskBase(BaseModel):
    title: str
    description: str
    deadline: datetime
    status: str


class TaskCreate(TaskBase):
    pass


class Task(TaskBase):
    id: int
    assignees: "List[User]" = []
    org: "Organization"

    class Config:
        orm_mode = True


# === organization ===

class OrganizationBase(BaseModel):
    name: str
    phone: str
    address: str


class OrganizationCreate(OrganizationBase):
    admin: "UserCreate"


class Organization(OrganizationBase):
    id: int
    # users: "List[User]" = []

    class Config:
        orm_mode = True


# update postponed references
User.update_forward_refs()
Task.update_forward_refs()
Organization.update_forward_refs()