from typing import List

from fastapi import Depends, FastAPI, HTTPException, status
from fastapi.security import HTTPBasic, HTTPBasicCredentials
from sqlalchemy.orm import Session

from . import models, schemas, crud, auth
from .database import SessionLocal, engine


models.Base.metadata.create_all(bind=engine)

app = FastAPI()

security = HTTPBasic()

# dependency
def get_db():
    db = SessionLocal()
    try:
        yield db
    finally:
        db.close()


# ===== organization =====


@app.post("/api/organizations", response_model=schemas.Organization)
def create_organization(org: schemas.OrganizationCreate, db: Session = Depends(get_db)):
    db_org = crud.get_org_by_name(db, org_name=org.name)
    if db_org:
        raise HTTPException(
            status_code=400, detail="Organization with that name already exists"
        )
    return crud.create_org(db, org=org)


@app.get("/api/organizations", response_model=List[schemas.Organization])
def get_organizations(skip: int = 0, limit: int = 100, db: Session = Depends(get_db)):
    orgs = crud.get_orgs(db, skip=skip, limit=limit)
    return orgs


@app.get("/api/organizations/my-org", response_model=schemas.Organization)
def get_current_organization(
    db: Session = Depends(get_db), creds: HTTPBasicCredentials = Depends(security)
):
    user = auth.auth_user(db, creds.username, creds.password)
    return user.org


# ===== user =====


@app.post("/api/users", response_model=schemas.User)
def create_user(
    user: schemas.UserCreate,
    db: Session = Depends(get_db),
    creds: HTTPBasicCredentials = Depends(security),
):
    req_user = auth.is_admin(db, creds.username, creds.password)
    if not req_user:
        raise HTTPException(
            status_code=status.HTTP_403_FORBIDDEN,
            detail="You are not an admin of any organization",
        )
    db_user = crud.get_user_by_email(db, user_email=user.email)
    if db_user:
        raise HTTPException(
            status_code=status.HTTP_400_BAD_REQUEST,
            detail="User with that email already exists",
        )
    return crud.create_user(db, user=user, user_org_id=req_user.org_id)


@app.delete("/api/users/{user_id}", response_model=schemas.User)
def delete_user(
    user_id: int,
    db: Session = Depends(get_db),
    creds: HTTPBasicCredentials = Depends(security),
):
    req_user = auth.is_admin(db, creds.username, creds.password)
    if not req_user:
        raise HTTPException(
            status_code=status.HTTP_403_FORBIDDEN, detail="Only admins can delete users"
        )
    user = crud.get_user(db, user_id)
    if not user:
        raise HTTPException(
            status_code=status.HTTP_404_NOT_FOUND,
            detail="User with that id does not exist",
        )
    crud.delete_user(db, user_id)
    return user


@app.get("/api/users", response_model=List[schemas.User])
def get_users(skip: int = 0, limit: int = 100, db: Session = Depends(get_db)):
    users = crud.get_users(db, skip=skip, limit=limit)
    return users


@app.get("/api/users/me", response_model=schemas.User)
def get_current_user(
    db: Session = Depends(get_db), creds: HTTPBasicCredentials = Depends(security)
):
    user = auth.auth_user(db, creds.username, creds.password)
    return user


# ===== task =====


@app.post("/api/tasks", response_model=schemas.Task)
def create_task(
    task: schemas.TaskCreate,
    db: Session = Depends(get_db),
    creds: HTTPBasicCredentials = Depends(security),
):
    req_user = auth.auth_user(db, creds.username, creds.password)
    user_id = req_user.id
    return crud.create_task(db, task, user_id, req_user.org_id)


@app.put("/api/tasks/{task_id}/add/{user_id}", response_model=schemas.Task)
def add_user_to_task(
    task_id: int,
    user_id: int,
    db: Session = Depends(get_db),
    creds: HTTPBasicCredentials = Depends(security),
):
    task = crud.get_task(db, task_id)
    if not task:
        raise HTTPException(
            status_code=status.HTTP_404_NOT_FOUND,
            detail="Task with that id does not exist",
        )
    user = crud.get_user(db, user_id)
    if not user:
        raise HTTPException(
            status_code=status.HTTP_404_NOT_FOUND,
            detail="User with that id does not exist",
        )
    if user.org_id != task.org_id:
        raise HTTPException(
            status_code=status.HTTP_403_FORBIDDEN,
            detail="Can't add user from another organization",
        )
    task = crud.add_user_to_task(db, user_id, task_id)
    return task


@app.delete("/api/tasks/{task_id}", response_model=schemas.Task)
def delete_task(
    task_id: int,
    db: Session = Depends(get_db),
    creds: HTTPBasicCredentials = Depends(security),
):
    req_user = auth.is_admin(db, creds.username, creds.password)
    if not req_user:
        raise HTTPException(
            status_code=status.HTTP_403_FORBIDDEN, detail="Only admins can delete tasks"
        )
    task = crud.get_task(db, task_id)
    if not task:
        raise HTTPException(
            status_code=status.HTTP_404_NOT_FOUND,
            detail="Task with that id does not exist",
        )
    crud.delete_user(db, task_id)
    return task


@app.get("/api/tasks/{task_id}", response_model=schemas.Task)
def get_task(
    task_id: int,
    db: Session = Depends(get_db),
    creds: HTTPBasicCredentials = Depends(security),
):
    user = auth.auth_user(db, creds.username, creds.password)
    task = crud.get_task(db, task_id)
    if not task or task.org_id != user.org_id:
        raise HTTPException(
            status_code=status.HTTP_404_NOT_FOUND,
            detail="Task with that id does not exist in your organization",
        )
    return task


@app.get("/api/tasks/me", response_model=List[schemas.Task])
def get_tasks_by_user(
    db: Session = Depends(get_db), creds: HTTPBasicCredentials = Depends(security)
):
    user = auth.auth_user(db, creds.username, creds.password)
    tasks = crud.get_tasks_by_user(db, user.id)
    return tasks


@app.get("/api/tasks/my-org", response_model=List[schemas.Task])
def get_tasks_by_org(
    db: Session = Depends(get_db), creds: HTTPBasicCredentials = Depends(security)
):
    user = auth.auth_user(db, creds.username, creds.password)
    tasks = crud.get_tasks_by_org(db, user.org_id)
    return tasks