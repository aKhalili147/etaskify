from fastapi import HTTPException, status
from sqlalchemy.orm import Session

from . import models, crud


def auth_user(db: Session, email: str, password: str):
    user = crud.get_user_by_email(db, email)
    if user and user.password == password:
        return user
    else:
        raise HTTPException(
            status_code=status.HTTP_401_UNAUTHORIZED,
            detail="Incorrect email or password"
        )


def is_admin(db: Session, email: str, password: str):
    user = auth_user(db, email, password)
    if user and user.is_admin:
        return user
    else:
        return None