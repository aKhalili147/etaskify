from sqlalchemy.orm import Session

from . import models, schemas


# ===== organization =====

def get_org(db: Session, org_id: int):
    return (
        db.query(models.Organization).filter(models.Organization.id == org_id).first()
    )


def get_org_by_name(db: Session, org_name: str):
    return (
        db.query(models.Organization)
        .filter(models.Organization.name == org_name)
        .first()
    )


def get_orgs(db: Session, skip: int = 0, limit: int = 100):
    return db.query(models.Organization).offset(skip).limit(limit).all()


def create_org(db: Session, org: schemas.OrganizationCreate):
    db_org = models.Organization(name=org.name, phone=org.phone, address=org.address)
    db.add(db_org)
    db.commit()
    db.refresh(db_org)

    admin_user = org.admin
    admin_user.is_admin = True
    create_user(db, admin_user, db_org.id)

    return db_org


# ===== user =====

def get_user(db: Session, user_id: int):
    return db.query(models.User).filter(models.User.id == user_id).first()


def get_user_by_email(db: Session, user_email: str):
    return db.query(models.User).filter(models.User.email == user_email).first()


def get_users(db: Session, skip: int = 0, limit: int = 100):
    return db.query(models.User).offset(skip).limit(limit).all()


def create_user(db: Session, user: schemas.UserCreate, user_org_id: int):
    db_user = models.User(
        name=user.name,
        surname=user.surname,
        email=user.email,
        # TODO: store hash, not plaintext
        password=user.password,
        is_admin=user.is_admin,
        org_id=user_org_id,
    )
    db.add(db_user)
    db.commit()
    db.refresh(db_user)
    return db_user


def delete_user(db: Session, user_id: int):
    db.query(models.User).filter(models.User.id == user_id).delete()
    db.commit()


# ===== task =====

def get_task(db: Session, task_id: int):
    return db.query(models.Task).filter(models.Task.id == task_id).first()


def get_tasks_by_user(db: Session, user_id: int):
    return db.query(models.Task).filter(
        models.Task.assignees.any(models.User.id == user_id)
    ).all()


def get_tasks_by_org(db: Session, org_id: int):
    return db.query(models.Task).filter(models.Task.org_id == org_id).all()


def create_task(db: Session, task: schemas.TaskCreate, user_id: int, user_org_id: int):
    db_task = models.Task(
        title=task.title,
        description=task.description,
        deadline=task.deadline,
        status=task.status,
        org_id=user_org_id,
    )
    db_task.assignees.append(get_user(db, user_id))
    db.add(db_task)
    db.commit()
    db.refresh(db_task)
    return db_task


def add_user_to_task(db, user_id: int, task_id: int):
    task = get_task(db, task_id)
    user = get_user(db, user_id)
    task.assignees.append(user)
    db.add(task)
    db.commit()
    db.refresh(task)
    return task


def delete_task(db: Session, task_id: int):
    db.query(models.Task).filter(models.Task.id == task_id).delete()
    db.commit()