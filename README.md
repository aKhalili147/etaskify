## Requirements

python 3.7 or later version need to be installed.

## From root directory:

pip3 install -r requirements.txt

## For server implementation:

uvicorn etaskify.main:app

### For API docs go to:
http://localhost:8000/docs
